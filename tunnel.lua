local tArgs = {...}
local x = 0
local y = 0
local z = 0
local dir = 0
local goingLeft = 0
local goingUp = 1
 
local function printPos()
 print(string.format("pos: (%d,%d,%d), dir: %d", x, y, z, dir))
end 

local function goForward()
 if turtle.forward() then
  if dir == 0 then
   z = z + 1
  elseif dir == 1 then
   x = x + 1
  elseif dir == 2 then
   z = z - 1
  elseif dir == 3 then
   x = x - 1
  end
  return true
 end
 return false
end

local function goDown()
 if turtle.down() then
  y = y - 1
  return true
 end
 return false
end

local function goUp()
 if turtle.up() then
  y = y + 1
  return true
 end
 return false
end

local function turnRight()
 turtle.turnRight()
 dir = dir + 1
 if dir > 3 then
  dir = 0
 end
end

local function turnLeft()
 turtle.turnLeft()
 dir = dir - 1
 if dir < 0 then
  dir = 3
 end
end

local function face(newDir)
 local delta = newDir - dir
 if delta > 2 or delta < 2 then
  while dir ~= newDir do
   turnLeft()
  end
 else
  while dir ~= newDir do
   turnRight()
  end
 end
end

local function digForward()
 while turtle.detect() do
  turtle.dig()
 end 
 if turtle.forward() then
  if dir == 0 then
   z = z + 1
  elseif dir == 1 then
   x = x + 1
  elseif dir == 2 then
   z = z - 1
  elseif dir == 3 then
   x = x - 1
  end
 end
end

local function digDown()
 while turtle.detectDown() do
  turtle.digDown()
 end
 goDown()
end

local function digUp()
 while turtle.detectUp() do
  turtle.digUp()
 end
 goUp()
end

local function digUpDown()
 while turtle.detectDown() do
  turtle.digDown()
 end
 while turtle.detectUp() do
  turtle.digUp()
 end
end

local function digHeight(height)
 if height == 2 then
  if turtle.detectDown then
   turtle.digDown()
  end
 elseif height == 3 then
  digUpDown()
 elseif height >= 4 then
  digUpDown()
  for i = 4, height do
   if goingUp == 1 then
    goUp()
    while turtle.detectUp() do
     turtle.digUp()
    end
   else
    goDown()
    while turtle.detectDown() do
     turtle.digDown()
    end
   end
  end
  if goingUp == 0 then
   goingUp = 1
  else
   goingUp = 0
  end
 end
end

local function tunnel(length, width, height)
 for i = 1,width do
  for j = 1,length do
   digHeight(height)
   digForward()
   printPos()
  end
  if goingLeft == 1 then
   turnLeft()
  else
   turnRight()
  end
 
  digHeight()
  if i < width then
   digForward()
  end
  printPos()

  if goingLeft == 1 then
   turnLeft()
   left = 0
  else
   turnRight()
   left = 1
  end
 end
end

local function goTo(newX, newY, newZ)
 while y > 0 do
  digDown()
  printPos()
 end

 while y < 0 do
  digUp()
  printPos()
 end

 if z > 0 then
  face(2)
  while z > 0 do
   digForward()
   printPos()
  end
 elseif z < 0 then
  face(0)
  while z < 0 do
   digForward()
   printPos()
  end
 end

 if x > 0 then
  face(3)
  while x > 0 do
   digForward()
   printPos()
  end
 elseif x < 0 then
  face(1)
  while x < 0 do
   digForward()
   printPos()
  end
 end
end

if #tArgs < 3 then
 print "Usage: tunnel <length> <width> <height> [l]"
 return
end

if #tArgs > 3 then
 if tArgs[4] == "l" then
  goingLeft = 1
 else
  goingLeft = 0
 end
end

if tonumber(tArgs[3]) > 1 then
 digUp()
 printPos()
end
tunnel(tonumber(tArgs[1]), tonumber(tArgs[2]), tonumber(tArgs[3]))
goTo(0, 0, 0)
