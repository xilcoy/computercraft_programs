local M_CMD = 0
local M_INS = 1
local M_REP = 2

local C_EDIT = 0
local C_CMD = 1
local C_SEARCH = 2

local K_BACK = 14
local K_RET = 28
local K_CTRL = 29
local K_HOME = 199
local K_UP = 200
local K_PGUP = 201
local K_LEFT = 203
local K_RIGHT = 205
local K_END = 207
local K_DOWN = 208
local K_PGDOWN = 209
local K_DEL = 211

local mode = M_CMD
local cmd_mode = C_EDIT
local searchDir = 0
local lastSearch = ""

local tArgs = { ... }

if #tArgs == 0 then
 print("Usage: vi <path>")
 return
end

local sPath = shell.resolve( tArgs[1] )
local bReadOnly = fs.isReadOnly( sPath )
if fs.exists( sPath ) and fs.isDir( sPath ) then
 print( "Cannot edit a directory" )
 return
end

local w,h = term.getSize()
local x,y = 1,1
local scrollX, scrollY = 0,0

local sStatus = ""
local tLines = {}

local function load()
 tLines = {}
 if fs.exists( sPath ) then
  local file = io.open( sPath, "r" )
  local sLine = file:read()
  while sLine do
   table.insert( tLines, sLine )
   sLine = file:read()
  end
  file:close()
 else
  table.insert( tLines, "" )
 end
end

local function save()
 local file = io.open( sPath, "w" )
 if file then
  for n, sLine in ipairs( tLines ) do
   file:write( sLine .. "\n" )
  end
  file:close()
  return true
 end
 return false
end

local function redrawText()
 for y=1,h-1 do
  term.setCursorPos( 1 - scrollX, y )
  term.clearLine()

  local sLine = tLines[ y + scrollY ]
  if sLine ~= nil then
   term.write( sLine )
  end
 end
 term.setCursorPos( x - scrollX, y - scrollY )
end

local function redrawLine()
 local sLine = tLines[y]
 term.setCursorPos( 1 - scrollX, y - scrollY )
 term.clearLine()
 term.write( sLine )
 term.setCursorPos( x - scrollX, y - scrollY )
end

local function setStatus( _sText )
 sStatus = _sText
 term.setCursorPos( 1, h )
 term.clearLine()
 term.write( sStatus )
 term.setCursorPos( x - scrollX, y - scrollY )
end

local function setCursor( x, y )
 local screenX = x - scrollX
 local screenY = y - scrollY

 local bRedraw = false
 if screenX < 1 then
  scrollX = x - 1
  screenX = 1
  bRedraw = true
 elseif screenX > w then
  scrollX = x - w
  screenX = w
  bRedraw = true
 end

 if screenY < 1 then
  scrollY = y - 1
  screenY = 1
  bRedraw = true
 elseif screenY > h-1 then
  scrollY = y - (h-1)
  screenY = h-1
  bRedraw = true
 end

 if bRedraw then
  redrawText()
 end
 term.setCursorPos( screenX, screenY )
 if mode == M_CMD then
  setStatus( string.format('"%s" %uL %u,%u', tArgs[1], #tLines, y, x ) )
 end
end

load()

term.clear()
term.setCursorPos(x,y)
term.setCursorBlink( true )

redrawText()
setStatus( string.format('"%s" %uL %u,%u', tArgs[1], #tLines, y, x ) )

local bRunning = true

local function cmdSave()
 if save() then
  setStatus(string.format('"%s" %uL written', sPath, #tLines))
  return true
 else
  setStatus("Access denied")
  return false
 end
end

local function cmdQuit()
 bRunning = false
end

local function cmdUp()
 if y > 1 then
  y = y - 1
  x = math.min( x, string.len( tLines[y] ) + 1 )
  setCursor( x, y )
 end
end

local function cmdDown()
 if y < #tLines then
 y = y + 1
 x = math.min( x, string.len( tLines[y] ) + 1 )
 setCursor( x, y )
 end
end

local function cmdLeft()
 if x > 1 then
  len = string.len( tLines[y] ) + 1
  if x > len then
   x = len 
  else
   x = x - 1
  end
  setCursor( x, y )
 end
end

local function cmdRight()
 if x >= string.len( tLines[y] ) + 1 then
  x = string.len( tLines[y] ) + 1
 else
  x = x + 1
 end
 setCursor( x, y )
end

local function cmdHome()
 x = 1
 setCursor( x, y )
end

local function cmdEnd()
 x = string.len( tLines[y] ) + 1
 setCursor( x, y )
end

local function cmdPgUp()
 y = y - h + 1
 if y < 1 then y = 1 end
 x = math.min( x, string.len( tLines[y] ) + 1 )
 setCursor( x, y )
end

local function cmdPgDown()
 y = y + h - 1
 if y > #tLines then y = #tLines end
 x = math.min( x, string.len( tLines[y] ) + 1 )
 setCursor( x, y )
end

local function cmdDelBefore(amount)
 if x > 1 then
  -- Remove character
  local sLine = tLines[y]
  tLines[y] = string.sub(sLine,1,x-2) .. string.sub(sLine,x)
  redrawLine()

  x = x - 1
  setCursor( x, y )

 elseif y > 1 then
  -- Remove newline
  local sPrevLen = string.len( tLines[y-1] )
  tLines[y-1] = tLines[y-1] .. tLines[y]
  table.remove( tLines, y )
  redrawText()

  x = sPrevLen + 1
  y = y - 1
  setCursor( x, y )
 end
end

local function cmdDelAfter(amount)
 if x <= string.len(tLines[y]) then
  -- Remove character
  local sLine = tLines[y]
  tLines[y] = string.sub(sLine,1,x-1) .. string.sub(sLine,x+1)
  redrawLine()

 elseif y < #tLines then
  -- Remove newline
  local sPrevLen = string.len( tLines[y] )
  tLines[y] = tLines[y] .. tLines[y+1]
  table.remove( tLines, y+1 )
  redrawText()

  x = sPrevLen + 1
  setCursor( x, y )
 end
end

local function insertNewLineBefore()
 local sLine = tLines[y]
 local sPrevTab = string.match( sLine, "%s+" )
 term.write(sPrevTab)
 tLines[y] = string.sub(sLine,1,x-1)
 if sPrevTab == nil then
  table.insert( tLines, y+1, string.sub(sLine,x) )
  x = 1
 else
  table.insert( tLines, y+1, sPrevTab .. string.sub(sLine,x) )
  x = #tLines[y+1]+1
 end
 redrawText()

 y = y + 1
 setCursor( x, y ) 
end

local function cmdOpenLineBefore()
 local sLine = tLines[y]
 local sPrevTab = string.match( sLine, "%s+" )
 term.write(sPrevTab)
 if sPrevTab == nil then
  table.insert( tLines, y-1, "" )
  x = 1
 else
  table.insert( tLines, y-1, sPrevTab)
  x = #tLines[y-1]+1
 end
 redrawText()

 setCursor( x, y ) 
 setStatus("-- INSERT --")
 mode = CMD_INSERT
end

local function cmdOpenLineAfter()
 local sLine = tLines[y]
 local sPrevTab = string.match( sLine, "%s+" )
 term.write(sPrevTab)
 if sPrevTab == nil then
  table.insert( tLines, y+1, "" )
  x = 1
 else
  table.insert( tLines, y+1, sPrevTab)
  x = #tLines[y+1]+1
 end
 redrawText()

 y = y + 1
 setCursor( x, y ) 
 setStatus("-- INSERT --")
 mode = CMD_INSERT
end

local function cmdFindNextPattern(pattern)
 cmd_mode = C_EDIT
 local pos = nil
 for i = y + 1, #tLines do
  pos = string.find(tLines[i], pattern)
  if pos then
   x = pos
   y = i
   setCursor(pos, i)
   return
  end
 end
 if pos == nil then
  for i = 1, y do
   local pos = string.find(tLines[i], pattern)
   if pos then
    x = pos
    y = i
    setCursor(pos, i)
    setStatus("search hit BOTTOM, continuing at TOP")
    return
   end
  end
 end
 if pos == nil then
  setStatus("Pattern not found: " .. pattern)
 end
end

local function cmdFindLastPattern(pattern)
 cmd_mode = C_EDIT
 local pos = nil
 for i = y - 1, 1, -1 do
  pos = string.find(tLines[i], pattern)
  if pos then
   x = pos
   y = i
   setCursor(pos, i)
   return
  end
 end
 if pos == nil then
  for i = #tLines, y, -1 do
   local pos = string.find(tLines[i], pattern)
   if pos then
    x = pos
    y = i
    setCursor(pos, i)
    setStatus("search hit TOP, continuing at BOTTOM")
    return
   end
  end
 end
 if pos == nil then
  setStatus("Pattern not found: " .. pattern)
 end
end

local command = {
 ["w"] = cmdSave,
 ["write"] = cmdSave,
 ["q"] = cmdQuit,
 ["quit"] = cmdQuit,
 ["wq"] = function() if cmdSave() then cmdQuit() end end,
 ["x"] = function() if cmdSave() then cmdQuit() end end,
}

while bRunning do
 local sEvent, param = os.pullEvent()
 if sEvent == "key" then
  if param == K_UP then cmdUp()
  elseif param == K_DOWN then cmdDown()
  elseif param == K_LEFT then cmdLeft()
  elseif param == K_RIGHT then cmdRight()
  elseif param == K_HOME then cmdHome()
  elseif param == K_END then cmdEnd()
  elseif param == K_PGUP then cmdPgUp()
  elseif param == K_PGDOWN then cmdPgDown()
  elseif param == K_BACK then
   if mode == M_INSERT then
    cmdDelBefore()
   elseif mode == M_CMD then
    cmdLeft()
   end
  elseif param == K_DEL then cmdDelAfter()
  elseif param == K_CTRL then
   if mode == M_INSERT then
    mode = M_CMD
    cmd_mode = C_EDIT
    setStatus( string.format('"%s" %uL %u,%u', tArgs[1], #tLines, y, x ) )
   end
  elseif param == K_RET then
   if mode == M_CMD then
    if cmd_mode == C_EDIT then
     cmdDown()
    elseif cmd_mode == C_CMD then
     local cString = string.sub(sStatus,2)
     if command[cString] == nil then
      setStatus("Not an editor command: " .. cString)
      cmd_mode = C_EDIT
     else
      local cmdFn = command[cString]
      cmdFn()
     end
    elseif cmd_mode == C_SEARCH then
     local sString = string.sub(sStatus,2)
     lastSearch = sString
     if searchDir == 0 then
      cmdFindNextPattern(sString)
     else
      cmdFindLastPattern(sString)
     end
    end
   else
    insertNewLineBefore()
   end
  end
 elseif sEvent == "char" then
  if mode == M_INSERT then
   -- Input text
   local sLine = tLines[y]
   tLines[y] = string.sub(sLine,1,x-1) .. param .. string.sub(sLine,x)
   redrawLine()

   x = x + string.len( param )
   setCursor( x, y )
  elseif mode == M_REPLACE then
   local tmp = 1
  elseif mode == M_CMD then
   if cmd_mode == C_EDIT then
    if     param == 'j' then cmdDown()
    elseif param == 'k' then cmdUp()
    elseif param == 'h' then cmdLeft()
    elseif param == 'l' then cmdRight()
    elseif param == 'x' then cmdDelAfter()
    elseif param == 'o' then cmdOpenLineAfter()
    elseif param == 'O' then cmdOpenLineBefore()
    elseif param == 'n' then
     if searchDir == 0 then
      cmdFindNextPattern(lastSearch)
     else
      cmdFindLastPattern(lastSearch)
     end
    elseif param == 'N' then
     if searchDir == 0 then
      cmdFindLastPattern(lastSearch)
     else
      cmdFindNextPattern(lastSearch)
     end
    elseif param == 'i' then
     setStatus("-- INSERT --")
     mode = CMD_INSERT
    elseif param == 'a' then
     setStatus("-- INSERT --")
     mode = CMD_INSERT
     x = x + 1
     setCursor(x, y)
    elseif param == 'R' then
     setStatus("-- REPLACE --")
     mode = CMD_REPLACE
    elseif param == ':' then
     cmd_mode = C_CMD
     setStatus(":")
    elseif param == '/' then
     cmd_mode = C_SEARCH
     searchDir = 0
     setStatus("/")
    elseif param == '?' then
     cmd_mode = C_SEARCH
     searchDir = 1
     setStatus("?")
    end
   else
    setStatus(sStatus .. param)
   end
  end
 end
end

term.clear()
term.setCursorBlink( false )
term.setCursorPos( 1, 1 )
